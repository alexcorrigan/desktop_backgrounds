#!/bin/sh

NEW_BACKGROUND=$1
DEFAULT_BACKGROUND_LINK="default_background"

rm $DEFAULT_BACKGROUND_LINK

ln -s $NEW_BACKGROUND $DEFAULT_BACKGROUND_LINK

feh --bg-fill ~/desktop_backgrounds/$DEFAULT_BACKGROUND_LINK
betterlockscreen -u ~/desktop_backgrounds/$DEFAULT_BACKGROUND_LINK
